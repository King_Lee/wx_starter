<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');//订单ID
            $table->integer('member_id')->unsigned();//会员ID
            $table->foreign('member_id')->references('id')->on('members')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('order_num')->unique();//订单号
            $table->string('wx_account')->nullable();//公众号账号名称
            $table->string('wx_password', 60)->nullable();//公众号账号密码
            $table->string('wx_name');//想要的公众号名称
            $table->string('wx_email')->nullable();//公众号登录邮箱
            $table->integer('wx_type')->nullable();//公众号账号类型
            $table->decimal('money')->nullable();//订单金额
            $table->integer('status');//订单状态
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
